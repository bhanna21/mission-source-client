const state = {
  missions: null,
  user: null
}

const getters = { }

const mutations = {
  setUser (state, username) {
    state.user = username
  },
  setMissions (state, missions) {
    state.missions = missions
  },
  LogOut (state) {
    state.user = null
    state.posts = null
  }
}

const actions = { }

export default [
  state,
  getters,
  mutations,
  actions
]
