import axios from 'axios'

const state = {
  missions: []
}

const getters = {
  getMissions: state => {
    return state.missions
  }
}
const mutations = {
  setMissions (state, data) {
    state.missions = data
  },
  addMission: (state, newMission) => state.missions.unshift(newMission)
}
const actions = {
  async fetchMissions ({ commit }) {
    const response = await axios.get('http://127.0.0.1:8000/api/missions')
    commit('setMissions', response.data)
  },

  async createMission ({ commit }, mission) {
    const response = await axios.post('http://127.0.0.1:8000/api/missions/new-mission', mission)
    commit('addMission', response.data)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
