import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import './plugins/element.js'
import VueRellax from 'vue-rellax'
import VueSmoothScroll from 'vue2-smooth-scroll'

Vue.config.productionTip = false
Vue.use(VueRellax)
Vue.use(VueSmoothScroll)

axios.defaults.baseURL = 'http://127.0.0.1:8000/api/'

// export const HTTP = axios.create({
//   baseURL: `http://jsonplaceholder.typicode.com/`,
//   headers: {
//     Authorization: 'Bearer {token}'
//   }
// })

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
