import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'
import Home from '../views/Home.vue'
import Dashboard from '../views/Dashboard.vue'
import CreateMission from '../views/CreateMission.vue'
import ProfilePage from '../views/ProfilePage.vue'
import LikedMissions from '../views/LikedMissions.vue'
import MissionDetails from '../views/MissionDetails.vue'
import Inbox from '../views/Inbox.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/sign-up',
    name: 'Sign Up',
    component: () => import('../views/Sign-Up.vue')
  },
  {
    path: '/sign-in',
    name: 'Sign In',
    component: () => import('../views/Sign-In.vue')
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard
    // meta: {
    //   requiresAuth: true
    // }
  },
  {
    path: '/mission/:id',
    name: 'Mission',
    component: MissionDetails
  },
  {
    path: '/create-mission',
    name: 'Create Mission',
    component: CreateMission
  },
  {
    path: '/profile',
    name: 'My profile',
    component: ProfilePage
  },
  {
    path: '/liked-missions',
    name: 'Liked Misisons',
    component: LikedMissions
  },
  {
    path: '/inbox',
    name: 'Your Inbox',
    component: Inbox
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isAuthenticated) {
      next()
      return
    }
    next('/sign-in')
  } else {
    next()
  }
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.guest)) {
    if (store.getters.isAuthenticated) {
      next('/dashboard')
      return
    }
    next()
  } else {
    next()
  }
})

export default router
